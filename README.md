## BO International Laravel Boilerplate
#### BO International Boilerplate is a Complete Build of Laravel 5.7 with  Social Authentication, User Roles and Permissions, User Profiles, and Admin restricted user management system and theme management 

### Installation Instructions
1. Run `git clone https://bointldev@bitbucket.org/bointldev/bosales.git`
`
2. Create a MySQL database for the project
    * ```mysql -u root -p```, if using Vagrant: ```mysql -u homestead -psecret```
    * ```create database laravelAuth;```
    * ```\q```

3.  cd bosales
4.  cd local 
5.  composer install
6.  run  `cp .env.example .env`
7.  Configure your `.env` file
8.  php artisan key:generate  
9.  sudo chmod -R 775 storage //for linux 
10. sudo chmod -R 775 bootrape //for linux 
11. php artisan db:seed


contact : ajayit2020@gmail.com


