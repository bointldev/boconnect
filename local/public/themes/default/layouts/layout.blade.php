<!DOCTYPE html>
<html lang="en">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="csrf-base" content="{{ URL::to('/') }}">
        <!--begin::Web font -->
    		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    		<script>
              WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
              });
    		</script>
        <!--end::Web font -->
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/default/assets/core/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}">
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/default/assets/core/vendors/base/vendors.bundle.css') }}">
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/default/assets/core/demo/default/base/style.bundle.css') }}">
        <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

        <title>@get('title')</title>

        <!-- @styles() -->

    </head>

    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(../../../assets/app/media/img//bg/bg-3.jpg);">
                    <div class="m-grid__item m-grid__item--fluid	m-login__wrapper">
                        <div class="m-login__container">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="https://s3.ap-south-1.amazonaws.com/cdnmaster/bo-logo.png" width="100%">
                                </a>
                            </div>
                            <div class="m-login__signin">                               
                                    <a href="{{ URL::to('/login')}}" class="btn btn-brand m-btn m-btn--custom m-btn--icon">
                                            <span>
                                                
                                                <span>Login</span>
                                            </span>
                                        </a>
                            </div>                      
                            
                            {{-- <div class="m-login__account">
                                <span class="m-login__account-msg">
                                    Don't have an account yet ?
                                </span>&nbsp;&nbsp;
                                <a href="javascript:void(0)" id="m_login_signup" class="m-link m-link--light m-login__account-link">Sign Up</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
    
            <!-- end:: Page -->

            

        <!-- @scripts() -->
        <!-- <script src="{{ asset('local/public/themes/admin/assets/core/vendors/langlist_datagrid.js') }}"></script> -->


        <script src="{{ asset('local/public/themes/default/assets/core/vendors/base/vendors.bundle.js') }}"></script>

        <script src="{{ asset('local/public/themes/default/assets/core/demo/default/base/scripts.bundle.js') }}"></script>
        <script src="{{ asset('local/public/themes/default/assets/core/vendors/hotkey.js') }}"></script>
        <script src="{{ asset('local/public/themes/default/assets/core/vendors/custom_admin.js') }}"></script>
        <script src="{{ asset('local/public/themes/default/assets/core/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
        <script src="{{ asset('local/public/themes/default/assets/core/app/js/dashboard.js') }}"></script>
        <script src="{{ asset('local/public/themes/default/assets/core/vendors/users_list.js') }}"></script>



        <script type="text/javascript">
        BASE_URL=$('meta[name="csrf-base"]').attr('content');
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        </script>

    </body>

</html>
