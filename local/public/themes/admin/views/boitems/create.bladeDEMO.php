<div class="m-content">
<div class="m-portlet m-portlet--mobile" style="border-radius:10px 10px 0 0">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Add New Items<small></small>
				</h3>
			</div>
        </div>
      
            


	</div>
	<div class="m-portlet__body">
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
        </ul>
      </div>
    @endif
		  <!-- form to save data  -->
      <form class="m-form m-form--label-align-left- m-form--state-"  action="{{ route('users.store')}}" method="post">
        @csrf
               <!--begin: Form Body -->
               <div class="m-portlet__body">
                   <!--begin: Form Wizard Step 1-->

                       <div class="row">
                           <div class="col-xl-8 offset-xl-2">
                               <div class="m-form__section m-form__section--first">
                                   <div class="m-form__heading">
                                       <h3 class="m-form__heading-title">Items Details</h3>
                                   </div>
                                   <div class="form-group m-form__group row">
                                       <label class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
                                       <div class="col-xl-9 col-lg-9">
                                           <input type="text" name="txtItems_name" class="form-control m-input" placeholder="">
                                           <span class="m-form__help">Please enter Items name</span>
                                           </div>
                                   </div>
                                   <div class="form-group m-form__group row">
                                       <label class="col-xl-3 col-lg-3 col-form-label">* Discription:</label>
                                       <div class="col-xl-9 col-lg-9">
                                           <input type="text" name="txtItems_discription" class="form-control m-input" placeholder="">
                                           <span class="m-form__help">Please enter Discription of the items</span>
                                       </div>
                                   </div>
                                   <div class="form-group m-form__group row">
                                       <label class="col-xl-3 col-lg-3 col-form-label">* Quantity(QTY)</label>
                                       <div class="col-xl-9 col-lg-9">
                                           <div class="input-group">
                                               <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key"></i></span></div>
                                               <input type="text" name="txtItems_qty" class="form-control m-input" placeholder="">
                                           </div>
                                           <span class="m-form__help">Enter quantity</span>
                                       </div>
                                   </div>
                                   <div class="form-group m-form__group row">
                                       <label class="col-xl-3 col-lg-3 col-form-label">*Attributes</label>
                                       <div class="col-xl-9 col-lg-9">
                                            <div class="form-group m-form__group">
                                                    <select class="form-control m-input select_item_attr" id="select_item_attr" name="txtItems_attr_name">
                                                        <option value="">--SELECT--</option>   
                                                        @foreach ($getAttr as $getAttrs)          
                                                            <option value="{{ $getAttrs->id }}">{{ $getAttrs->attr_name }}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                           <span class="m-form__help">Select Attributes of the items like color ,size,weight</span>
                                       </div>
                                   </div>
                                   <div class="form-group m-form__group row select_item_attr_values_active">
                                        <label class="col-xl-3 col-lg-3 col-form-label">*Attributes Values</label>
                                        <div class="col-xl-9 col-lg-9">
                                             <div class="form-group m-form__group">
                                                     <select class="form-control m-input select_item_attr_values" id="select_item_attr_values"  name="txtItems_attr_values">
                                                            <option value="">--SELECT--</option>   
                                                     </select>
                                                 </div>
                                            <span class="m-form__help">Select Attributes values of the items like red,20mm,200ml respectively</span>
                                        </div>
                                    </div>

                                   

                               </div>
                               <div class="m-separator m-separator--dashed m-separator--lg"></div>
                               
                                  <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                 <div class="row">
										<div class="col-lg-6 m--align-left">
											<button type="button" class="btn btn-primary m-btn m-btn m-btn--icon backMe">
											<span>
											<i class="la la-arrow-left"></i>
											<span>Back</span>

											</span>
                                        </button>
										</div>
										<div class="col-lg-6 m--align-right">

											<button type ="button" id="btnSaveItems" class="btn btn-warning m-btn m-btn m-btn--icon">
											<span>
											<span>Save</span>
											<i class="la la-save"></i>
											</span>
                                            </button>
										</div>
									</div>

                           </div>
                       </div>

                 </div>
               </div>


          <!-- form to save data  -->
         

	
</div>
<div class="m_datatable_bo_items_list" id="ajax_data"></div>



</div>
</div>
</div>
<!-- end:: Body -->
