<div class="m-content">
<div class="m-portlet m-portlet--mobile" style="border-radius:10px 10px 0 0">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Add New Items<small></small>
				</h3>
			</div>
        </div>   


	</div>
	<div class="m-portlet__body">    
		<!-- form to save data  -->    
       
									<form class="m-form m-form--label-align-left- m-form--state-" id="m_form_submit" action ="{{route('boitems.store')}}" method="post">
                                        @csrf
										<!--begin: Form Body -->
										<div class="m-portlet__body">                                        
                                            <!----------------------->
                                            <div class="row">
                                                <div class="col-xl-8 offset-xl-2">
                                                    <div class="m-form__section m-form__section--first">
                                                        <div class="m-form__heading">
                                                            <h3 class="m-form__heading-title">Items Details</h3>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">* Name:</label>
                                                            <div class="col-xl-9 col-lg-9">
                                                                <input type="text" name="txtItems_name" class="form-control m-input" placeholder="">
                                                                <span class="m-form__help">Please enter items name</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">* Discriptions:</label>
                                                            <div class="col-xl-9 col-lg-9">
                                                                <input type="text" name="txtItems_discription" class="form-control m-input" placeholder="" value="nick.stone@gmail.com">
                                                                <span class="m-form__help">Please Enter Discriptions</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">* Quantity</label>
                                                            <div class="col-xl-9 col-lg-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"></span></div>
                                                                    <input type="text" name="txtItems_qty" class="form-control m-input" placeholder="">
                                                                </div>
                                                                <span class="m-form__help">Quantity E.g: 1</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">*Attributes</label>
                                                            <div class="col-xl-9 col-lg-9">
                                                                 <div class="form-group m-form__group">
                                                                         <select class="form-control m-input select_item_attr" id="select_item_attr" name="txtItems_attr_name">
                                                                             <option value="">--SELECT--</option>   
                                                                             @foreach ($getAttr as $getAttrs)          
                                                                                 <option value="{{ $getAttrs->id }}">{{ $getAttrs->attr_name }}</option>
                                                                                 @endforeach
                                                                         </select>
                                                                     </div>
                                                                <span class="m-form__help">Select Attributes of the items like color ,size,weight</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row select_item_attr_values_active">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">*Attributes Values</label>
                                                            <div class="col-xl-9 col-lg-9">
                                                                 <div class="form-group m-form__group">
                                                                         <select class="form-control m-input select_item_attr_values" id="select_item_attr_values"  name="txtItems_attr_values">
                                                                                <option value="">--SELECT--</option>   
                                                                         </select>
                                                                     </div>
                                                                <span class="m-form__help">Select Attributes values of the items like red,20mm,200ml respectively</span>
                                                            </div>
                                                        </div>                                                     


                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <!------------------------>
                                            
										</div>

										<!--end: Form Body -->

										<!--begin: Form Actions -->
										<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
											<div class="m-form__actions">
												<div class="row">
													<div class="col-lg-2"></div>
													<div class="col-lg-4 m--align-left">
														<a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
															<span>
																<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																<span>Back</span>
															</span>
														</a>
													</div>
													<div class="col-lg-4 m--align-right">
														<a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
															<span>
																<i class="la la-check"></i>&nbsp;&nbsp;
																<span>Submit</span>
															</span>
														</a>
														
													</div>
													<div class="col-lg-2"></div>
												</div>
											</div>
										</div>

										<!--end: Form Actions -->
									</form>
								

								

        <!-- form to save data  -->
        </div>
    </div>
<div class="m_datatable_bo_items_list" id="ajax_data"></div>



</div>
</div>
</div>
<!-- end:: Body -->
