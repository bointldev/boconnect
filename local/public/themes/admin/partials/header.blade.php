<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
  <!-- BEGIN: Header -->
  <header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
    <div class="m-container m-container--fluid m-container--full-height">
      <div class="m-stack m-stack--ver m-stack--desktop">
        <!-- BEGIN: Brand -->
        <div class="m-stack__item m-brand  m-brand--skin-dark ">
          <div class="m-stack m-stack--ver m-stack--general">
            <div class="m-stack__item m-stack__item--middle m-brand__logo">
              <a href="../index.html" class="m-brand__logo-wrapper">
                <img alt="logo"  width="80%" src="https://s3.ap-south-1.amazonaws.com/cdnmaster/bo-logo.png"/>
              </a>
            </div>          
          </div>
        </div>
        <!-- END: Brand -->
        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
          <!-- BEGIN: Horizontal Menu -->
         
          
          <!-- END: Horizontal Menu -->								<!-- BEGIN: Topbar -->
          <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
            <div class="m-stack__item m-topbar__nav-wrapper">
              <ul class="m-topbar__nav m-nav m-nav--inline">
                
               
                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                    <span class="m-topbar__userpic">
                      <img src="https://www.shareicon.net/data/2016/08/05/806962_user_512x512.png" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                    </span>
                    <span class="m-topbar__username m--hide">
                      Nick
                    </span>
                  </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__header m--align-center" style="background: url(../assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                        <div class="m-card-user m-card-user--skin-dark">
                          <div class="m-card-user__pic">
                            <img src="https://www.shareicon.net/data/2016/08/05/806962_user_512x512.png" class="m--img-rounded m--marginless" alt=""/>
                          </div>
                          <div class="m-card-user__details">
                            <span class="m-card-user__name m--font-weight-500">
                              {{ Auth::user()->name}}
                            </span>
                            <a href="" class="m-card-user__email m--font-weight-300 m-link">
                             {{ Auth::user()->email}}
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="m-dropdown__body">
                        <div class="m-dropdown__content">
                          <ul class="m-nav m-nav--skin-light">
                            <li class="m-nav__section m--hide">
                              <span class="m-nav__section-text">
                                Section
                              </span>
                            </li>
                            <li class="m-nav__item">
                              <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                <span class="m-nav__link-title">
                                  <span class="m-nav__link-wrap">
                                    <span class="m-nav__link-text">
                                      My Profile
                                    </span>
                                    <span class="m-nav__link-badge">
                                      <span class="m-badge m-badge--success">
                                        
                                      </span>
                                    </span>
                                  </span>
                                </span>
                              </a>
                            </li>
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="Logout">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();                                                   document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                              </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                
              </ul>
            </div>
          </div>
          <!-- END: Topbar -->
        </div>
      </div>
    </div>
  </header>
  <!-- END: Header -->
