<!DOCTYPE html>
<html lang="en">

    <head>
        {!! meta_init() !!}
        <meta name="keywords" content="@get('keywords')">
        <meta name="description" content="@get('description')">
        <meta name="author" content="@get('author')">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="csrf-base" content="{{ URL::to('/') }}">
        <!--begin::Web font -->
    		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    		<script>
              WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
              });
    		</script>
        <!--end::Web font -->
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/admin/assets/core/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}">
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/admin/assets/core/vendors/base/vendors.bundle.css') }}">
        <link media="all" type="text/css" rel="stylesheet" href="{{ asset('local/public/themes/admin/assets/core/demo/default/base/style.bundle.css') }}">
        <title>@get('title')</title>

        <!-- @styles() -->

    </head>

    <body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

        @partial('header')
        @partial('left_sidebar')
        @content()
        @partial('right_sidebar')
        @partial('footer')
        <!-- @scripts() -->
        <!-- <script src="{{ asset('local/public/themes/admin/assets/core/vendors/langlist_datagrid.js') }}"></script> -->


        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/base/vendors.bundle.js') }}"></script>

        <script src="{{ asset('local/public/themes/admin/assets/core/demo/default/base/scripts.bundle.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/hotkey.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/custom_admin.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/app/js/dashboard.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/users_list.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/roles_list.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/bo_items_list.js') }}"></script>
        
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/toastr.js') }}"></script>
        <script src="{{ asset('local/public/themes/admin/assets/core/vendors/boitem_save_wizard.js') }}"></script>

        <script type="text/javascript">
        BASE_URL=$('meta[name="csrf-base"]').attr('content');
        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        //select attr values ajax call
        $('.select_item_attr_values_active').hide();
        $("select.select_item_attr").change(function(){
        var select_item_attr = $(this).children("option:selected").val();
        $.ajax({
            url:BASE_URL+"/api/get_attr_items",
            type: 'POST',
            data: {_token: CSRF_TOKEN,select_item_attr:select_item_attr},
            success: function (resp) {              
                $('.select_item_attr_values').html("");
                $('.select_item_attr_values').html(`
                    <option>--SELECT--</option>
                `);
                
                $.each(resp.data, function(k, v) {
                $('.select_item_attr_values').append(`
                    <option value="${v.id}">${v.attr_value}</option>
                `);
                });
                $('.select_item_attr_values_active').show();
            }
        });

        });

        //save items ajax based save
        $('#btnSaveItems').click(function(){
            var txtItems_name= $('input[name="txtItems_name"]').val();
            var txtItems_discription= $('input[name="txtItems_discription"]').val();
            var txtItems_qty= $('input[name="txtItems_qty"]').val();            
            var select_item_attr = $(".select_item_attr option:selected").val();
            var select_item_attr_values = $(".select_item_attr_values option:selected").val();

            //validations
            
            //validations
            $.ajax({
            url:BASE_URL+"/api/save_attr_values",
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                txtItems_name:txtItems_name,
                txtItems_discription:txtItems_discription,
                select_item_attr:select_item_attr,
                txtItems_qty:txtItems_qty,
                select_item_attr_values:select_item_attr_values            },
            success: function (resp) {   
                console.log(resp.status);           
                if(resp.status==0){       
                    swal("Alret!", `Already added ${txtItems_name}`, "warning");
                    
                }else{                   
                    swal("Success!", `Successfully added  ${txtItems_name}`, "success");

                    setTimeout(function() {
                            location.reload();
                }, 5000);               
                }
            } 
        });



            
        });

        //save items ajax based save


        </script>

    </body>

</html>
