<div class="m-content">
<!-- order create -->
<div class="row">
  <div class="col-md-4">
       <!-- add item -->
						<!--begin::Portlet-->
						<div class="m-portlet" data-select2-id="83">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										        Purchase Order
										</h3>
									</div>
								</div>
							</div>
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" data-select2-id="82">
								<div class="m-portlet__body" data-select2-id="81">
                  <div class="form-group m-form__group row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                      <div class="form-group m-form__group row">
												<label for="example-text-input" class="col-2 col-form-label">PID</label>
												<div class="col-12">
													<input class="form-control m-input" disabled type="text" value="BO-219-12542" id="example-text-input">
												</div>
											</div>

                  </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                      <div class="form-group m-form__group row">
												<label for="example-text-input" class="col-2 col-form-label">Date:</label>
												<div class="col-12">
													<input type="text" class="form-control" id="m_datepicker_1" readonly placeholder="Select date" />
												</div>
											</div>
                    </div>
                </div>
                  <div class="form-group m-form__group row">
                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <?php
                    $arr_vendor=AyraHelp::getVendorName();
                    $arr_BOMaterial=AyraHelp::getBOMaterial();


                    ?>

                    <select class="form-control m-select2" id="m_select2_2" name="param">
												  <option></option>
                          @foreach ($arr_vendor as $arr_vendors)
                              <option value="{{ $arr_vendors->id }}">{{ $arr_vendors->name }}</option>
                          @endforeach
											</select>


                    </div>
                    <div class="col-md-2">
                      <a href="#" class="btn btn-info m-btn m-btn--icon m-btn--icon-only">
                              <i class="fa flaticon-plus"></i>
                      </a>
                    </div>
                </div>

                  <div class="form-group m-form__group row">
                  <div class="col-lg-10 col-md-10 col-sm-10">
                    <select class="form-control m-select2 ajItemSelect" id="m_select2_4" name="param">
												  <option></option>
                          @foreach ($arr_BOMaterial as $arr_BOMaterials)
                              <option value="{{ $arr_BOMaterials->item_code }}">{{ $arr_BOMaterials->item_name }}</option>
                          @endforeach

											</select>

                    </div>
                    <div class="col-md-2">
                      <a href="#" class="btn btn-info m-btn m-btn--icon m-btn--icon-only">
                              <i class="fa flaticon-plus"></i>
                      </a>

                    </div>

                </div>
                <hr>
                <div class="m-section">
                          <div class="m-section__content">
                            <table id="itemsTable" class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand">
                              <thead>
                                <tr>
                                 <th>ITEM NAME</th>
                                  <th>QTY</th>
                                  <th>ACTION</th>
                                </tr>
                              </thead>
                              <tbody id="tbody">


                              </tbody>
                            </table>
                          </div>
              </div>
                <div class="form-group m-form__group">
                    <label for="exampleTextarea">Remarks</label>
                    <textarea class="form-control m-input" id="exampleTextarea" rows="3"></textarea>
                </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="reset" class="btn btn-primary">Submit</button>
												<button type="reset" class="btn btn-secondary">Cancel</button>
											</div>
										</div>



								</div>
							</form>
							<!--end::Form-->

						</div>
						<!--end::Portlet-->








       <!-- add item -->
  </div>
  <div class="col-md-8">

    <!-- add item -->
         <!--begin::Portlet-->
         <div class="m-portlet" data-select2-id="83">
           <div class="m-portlet__head">
             <div class="m-portlet__head-caption">
               <div class="m-portlet__head-title">
                 <h3 class="m-portlet__head-text">
                         Stock Details
                 </h3>
               </div>
             </div>
           </div>
           <!--begin::Form-->
           <div class="m-portlet m-portlet--mobile">
             <div class="m-portlet__head">
               <div class="m-portlet__head-caption">
                 <div class="m-portlet__head-title">
                   <h3 class="m-portlet__head-text">
                     AJAX Datatable<small>data loaded from remote data source</small>
                   </h3>
                 </div>
               </div>
               <div class="m-portlet__head-tools">
                 <ul class="m-portlet__nav">
                   <li class="m-portlet__nav-item">
                     <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                       <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                         <i class="la la-ellipsis-h m--font-brand"></i>
                       </a>
                       <div class="m-dropdown__wrapper">
                         <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                         <div class="m-dropdown__inner">
                           <div class="m-dropdown__body">
                             <div class="m-dropdown__content">
                               <ul class="m-nav">
                                 <li class="m-nav__section m-nav__section--first">
                                   <span class="m-nav__section-text">Quick Actions</span>
                                 </li>
                                 <li class="m-nav__item">
                                   <a href="" class="m-nav__link">
                                     <i class="m-nav__link-icon flaticon-share"></i>
                                     <span class="m-nav__link-text">Create Post</span>
                                   </a>
                                 </li>
                                 <li class="m-nav__item">
                                   <a href="" class="m-nav__link">
                                     <i class="m-nav__link-icon flaticon-chat-1"></i>
                                     <span class="m-nav__link-text">Send Messages</span>
                                   </a>
                                 </li>
                                 <li class="m-nav__item">
                                   <a href="" class="m-nav__link">
                                     <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                     <span class="m-nav__link-text">Upload File</span>
                                   </a>
                                 </li>
                                 <li class="m-nav__section">
                                   <span class="m-nav__section-text">Useful Links</span>
                                 </li>
                                 <li class="m-nav__item">
                                   <a href="" class="m-nav__link">
                                     <i class="m-nav__link-icon flaticon-info"></i>
                                     <span class="m-nav__link-text">FAQ</span>
                                   </a>
                                 </li>
                                 <li class="m-nav__item">
                                   <a href="" class="m-nav__link">
                                     <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                     <span class="m-nav__link-text">Support</span>
                                   </a>
                                 </li>
                                 <li class="m-nav__separator m-nav__separator--fit m--hide">
                                 </li>
                                 <li class="m-nav__item m--hide">
                                   <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                 </li>
                               </ul>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </li>
                 </ul>
               </div>
             </div>
             <div class="m-portlet__body">

               <!--begin: Search Form -->
               <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                 <div class="row align-items-center">
                   <div class="col-xl-12 order-2 order-xl-1">
                     <div class="form-group m-form__group row align-items-center">
                       <div class="col-md-4">
                         <div class="m-form__group m-form__group--inline">
                           <div class="m-form__label">
                             <label>Status:</label>
                           </div>
                           <div class="m-form__control">
                             <select class="form-control m-bootstrap-select" id="m_form_status">
                               <option value="">All</option>
                               <option value="1">Pending</option>
                               <option value="2">Delivered</option>
                               <option value="3">Canceled</option>
                             </select>
                           </div>
                         </div>
                         <div class="d-md-none m--margin-bottom-10"></div>
                       </div>
                       <div class="col-md-4">
                         <div class="m-form__group m-form__group--inline">
                           <div class="m-form__label">
                             <label class="m-label m-label--single">Type:</label>
                           </div>
                           <div class="m-form__control">
                             <select class="form-control m-bootstrap-select" id="m_form_type">
                               <option value="">All</option>
                               <option value="1">Online</option>
                               <option value="2">Retail</option>
                               <option value="3">Direct</option>
                             </select>
                           </div>
                         </div>
                         <div class="d-md-none m--margin-bottom-10"></div>
                       </div>
                       <div class="col-md-4">
                         <div class="m-input-icon m-input-icon--left">
                           <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                           <span class="m-input-icon__icon m-input-icon__icon--left">
                             <span><i class="la la-search"></i></span>
                           </span>
                         </div>
                       </div>
                     </div>
                   </div>

                 </div>
               </div>

               <!--end: Search Form -->

               <!--begin: Datatable -->
               <div class="m_datatable_BOSTOCK" id="ajax_data"></div>

               <!--end: Datatable -->
             </div>
           </div>
           <!--end::Form-->

         </div>
         <!--end::Portlet-->








    <!-- add item -->


  </div>
</div>
<!-- order create -->




</div>
</div>
</div>
<!-- end:: Body -->
