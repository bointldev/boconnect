<div class="m-content">
<div class="m-portlet m-portlet--mobile" style="border-radius:10px 10px 0 0">
	<!--begin::Form m_form_3-->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="m_form_3" method="post" action="{{ route('saveClient')}}">
        @csrf
        <!--begin: Portlet Head-->
        <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Client Registration
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Get help with filling up this form">
                                <i class="flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
        <!--end: Portlet Head-->
    <!--begin: Form Wizard Step 1-->
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* First Name:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="userfirstName" autocomplete="off" class="form-control m-input" placeholder="" >
                            <span class="m-form__help">Please enter your first name</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Last Name:</label>
                            <div class="col-xl-9 col-lg-9">
                                <input type="text" name="userlastName" autocomplete="off"  class="form-control m-input" placeholder="" >
                                <span class="m-form__help">Please enter your last name</span>
                            </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* Email:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="email" name="email" autocomplete="off" class="form-control m-input" placeholder="" >
                            <span class="m-form__help">We'll never share your email with anyone else</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">*Phone</label>
                        <div class="col-xl-9 col-lg-9">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                <input type="text" name="userPhone" autocomplete="off" class="form-control m-input" placeholder="">
                            </div>
                            <span class="m-form__help">Enter your valid  contact numbers</span>
                        </div>
                    </div>
                </div>
                <div class="m-separator m-separator--dashed m-separator--lg"></div>
                <div class="m-form__section">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            Address
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Some help text goes here"></i>
                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* Address Line 1:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="user_address_1" class="form-control m-input" placeholder="" >
                            <span class="m-form__help">Street address, P.O. box, company name, c/o</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Address Line 2:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="address2" class="form-control m-input" placeholder="" >
                            <span class="m-form__help">Appartment, suite, unit, building, floor, etc</span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">* State:</label>
                            <div class="col-xl-9 col-lg-9">
                                <input type="text" name="user_state" class="form-control m-input" placeholder="" >
                            </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">* City:</label>
                        <div class="col-xl-9 col-lg-9">
                            <input type="text" name="user_city" class="form-control m-input" placeholder="" >
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--end: Form Wizard Step 1-->

        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-accent">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--end::Form-->


</div>
</div>
</div>
</div>
<!-- end:: Body -->
