<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
			<i class="la la-close"></i>
		</button>
		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
			<!-- BEGIN: Aside Menu -->
<div  id="m_ver_menu"
class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500"		>
				<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
					<li class="m-menu__section m-menu__section--first" style="background:#00c5dc;">
						<h4 class="m-menu__section-text">
							<strong style="color:#FFF">{{ Auth::user()->name}}</strong>
						</h4>
						<i class="m-menu__section-icon flaticon-more-v3"></i>
					</li>
					<!--this is sample-->
					<li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--open" aria-haspopup="true"  m-menu-submenu-toggle="hover">
						<a  href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon flaticon-layers"></i>
							<span class="m-menu__link-text">
								Clients
							</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu ">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
									<span class="m-menu__link">
										<span class="m-menu__link-text">
											Clients
										</span>
									</span>
								</li>
								<li class="m-menu__item  m-menu__item" aria-haspopup="true" >
									<a  href="#" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Client List
										</span>
									</a>
								</li>
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="{{ route('addNewClient')}}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Add New Client
										</span>
									</a>
								</li>
								
							</ul>
						</div>
					</li>
					<!--this is sample-->

					<!--this is sample-->
					<li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--open" aria-haspopup="true"  m-menu-submenu-toggle="hover">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-icon flaticon-layers"></i>
								<span class="m-menu__link-text">
									Orders
								</span>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu ">
								<span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
										<span class="m-menu__link">
											<span class="m-menu__link-text">
												Orders
											</span>
										</span>
									</li>
									<li class="m-menu__item  m-menu__item" aria-haspopup="true" >
										<a  href="{{ route('client-Order.index')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Order List
											</span>
										</a>
									</li>
									<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
										<a  href="{{ route('client-Order.create')}}" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
												Add New
											</span>
										</a>
									</li>
									
								</ul>
							</div>
						</li>
						<!--this is sample-->

						<!--this is sample-->
					<li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--open" aria-haspopup="true"  m-menu-submenu-toggle="hover">
							<a  href="javascript:;" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-icon flaticon-layers"></i>
								<span class="m-menu__link-text">
									Delivered Orders
								</span>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							</a>
							<div class="m-menu__submenu ">
								<span class="m-menu__arrow"></span>
								<ul class="m-menu__subnav">
									<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
										<span class="m-menu__link">
											<span class="m-menu__link-text">
												Delivered Orders
											</span>
										</span>
									</li>
									<li class="m-menu__item  m-menu__item" aria-haspopup="true" >
										<a  href="#" class="m-menu__link ">
											<i class="m-menu__link-bullet m-menu__link-bullet--dot">
												<span></span>
											</i>
											<span class="m-menu__link-text">
													Delivered  List
											</span>
										</a>
									</li>
									
									
								</ul>
							</div>
						</li>
						<!--this is sample-->				
					
					<li class="m-menu__section" style="display:none">
						<h4 class="m-menu__section-text">
							Roles and Permissions 
						</h4>
						<i class="m-menu__section-icon flaticon-more-v3"></i>
					</li>

					<?php
				$route_name=AyraHelp::getRouteName();

					?>
					<?php
					if(str_split($route_name,5)[0]=='users'){
						$menu_class='m-menu__item m-menu__item--submenu m-menu__item--open m-menu__item--hover';
						$menu_style="display: block; overflow: hidden;";
					}else{
						$menu_class='m-menu__item  m-menu__item--submenu';
						$menu_style="";
					}
					?>

					<li style="display:none" class="{{$menu_class}}" aria-haspopup="true"  m-menu-submenu-toggle="hover">
						<a  href="javascript:;" class="m-menu__link m-menu__toggle">
							<i class="m-menu__link-icon flaticon-clipboard"></i>
							<span class="m-menu__link-text">
								Manager Users
								<span class="m-menu__link-badge">
									<span class="m-badge m-badge--accent">
								{{AyraHelp::getNewUserCount()}}
									</span>
								</span>
							</span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
						</a>
						<div class="m-menu__submenu " style="{{$menu_style}}">
							<span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
									<span class="m-menu__link">
										<span class="m-menu__link-text">
											User Management
										</span>

									</span>
								</li>
								<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
									<a  href="{{ route('users.index')}}" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											Users
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--accent">
												  {{AyraHelp::getNewUserCount()}}
												</span>
											</span>
										</span>
									</a>
								</li>
								
								
							</ul>
						</div>
					</li>
				</ul>
			</div>
			<!-- END: Aside Menu -->
		</div>
		<!-- END: Left Aside -->
		<div class="m-grid__item m-grid__item--fluid m-wrapper">
