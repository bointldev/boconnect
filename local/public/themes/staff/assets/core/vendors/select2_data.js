//== Class definition
var Select2 = function() {
    //== Private functions
    var demos = function() {
        // basic
        // $(".ajItemSelect").keyup(function(event){
        //  var searchVal = $(this).val();
        //  specialCharacters = ['~','!','@','#','$','%','^','&','*','(',')','-','+','_','/','\\','|','{','}'];
        //  if($.inArray(searchVal.charAt(0), specialCharacters) !== -1){
        //      $(this).val(searchVal.substr(1));
        //  }
        // });

        $('#itemsTable').hide();
        $("select.ajItemSelect").change(function(){
          var item = $(this).children("option:selected").val();
          var text = $(this).children("option:selected").text();
          if(text==""){
            return false;
          }
          var arr = $("input[name='p_code[]']")
                .map(function(){return $(this).val();}).get();
              //  console.log(values);
              $('#itemsTable').show();

          for(var i=0; i<arr.length; i++){
            var name = arr[i];
            if(name == item){
            console.log('exits');
            idcode='#'+item;
            var itemV=$(idcode).val();
            var itemV4= parseInt(itemV)+1;
            $(idcode).val(itemV4);
             return false;
            }
          }

          $('#tbody').append(`
            <tr>
              <td><input type="hidden"  name="p_code[]" value="${item}"> ${text}</td>
              <td><input name="" id="${item}"  value="1" size="1"></td>
              <td class="removeMe"><a href="javascript:void(0)"><i class="flaticon-delete-1"></i></a></td>
            </tr>`);
            var values = $("input[name='p_code[]']")
                  .map(function(){return $(this).val();}).get();
                //  console.log(values);
                $('.removeMe').click(function(){
                  $(this).parent().remove();
                });


        });

        $('#m_select2_1, #m_select2_1_validate').select2({
            placeholder: "Select a vendor"
        });

        // nested
        $('#m_select2_2, #m_select2_2_validate').select2({
            placeholder: "Select a vendor"
        });

        // multi select
        $('#m_select2_3, #m_select2_3_validate').select2({
            placeholder: "Select a vendor",
        });

        // basic
        $('#m_select2_4').select2({
            placeholder: "Select products/Code",
            allowClear: true
        });
        //packaging
        $('#m_select2_41').select2({
            placeholder: "Select a Packaging",
            allowClear: true
        });


        // loading data from array
        var data = [{
            id: 0,
            text: 'Enhancement'
        }, {
            id: 1,
            text: 'Bug'
        }, {
            id: 2,
            text: 'Duplicate'
        }, {
            id: 3,
            text: 'Invalid'
        }, {
            id: 4,
            text: 'Wontfix'
        }];

        $('#m_select2_5').select2({
            placeholder: "Select a value",
            data: data
        });

        // loading remote data

        function formatRepo(repo) {
            if (repo.loading) return repo.text;
            var markup = `<div class="m-widget4">
            <div class="m-widget4__item">
              <div class="m-widget4__img m-widget4__img--logo">
                <img src="https://www.w3schools.com/w3images/avatar6.png" alt="">
              </div>
              <div class="m-widget4__info">
                <span class="m-widget4__title">
                  Trump Themes
                </span><br>
                <span class="m-widget4__sub">
                  Make Metronic Great Again
                </span>
              </div>
              <span class="m-widget4__ext">
                <span class="m-widget4__number m--font-brand">+$2500</span>
              </span>
            </div>
          </div>`;
          return markup;
        }
        // function formatRepo(repo) {
        //     if (repo.loading) return repo.text;
        //     var markup = "<div class='select2-result-repository clearfix'>" +
        //         "<div class='select2-result-repository__meta'>" +
        //         "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
        //     if (repo.description) {
        //         markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        //     }
        //     markup += "<div class='select2-result-repository__statistics'>" +
        //         "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
        //         "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
        //         "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
        //         "</div>" +
        //         "</div></div>";
        //     return markup;
        // }

        function formatRepoSelection(repo) {
            return repo.full_name || repo.text;
        }

        $("#m_select2_order").select2({
            placeholder: "Search Products",
            allowClear: true,
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        // custom styles

        // tagging support
        $('#m_select2_12_1, #m_select2_12_2, #m_select2_12_3, #m_select2_12_4').select2({
            placeholder: "Select an option",
        });

        // disabled mode
        $('#m_select2_7').select2({
            placeholder: "Select an option"
        });

        // disabled results
        $('#m_select2_8').select2({
            placeholder: "Select an option"
        });

        // limiting the number of selections
        $('#m_select2_9').select2({
            placeholder: "Select an option",
            maximumSelectionLength: 2
        });

        // hiding the search box
        $('#m_select2_10').select2({
            placeholder: "Select an option",
            minimumResultsForSearch: Infinity
        });

        // tagging support
        $('#m_select2_11').select2({
            placeholder: "Add a tag",
            tags: true
        });

        // disabled results
        $('.m-select2-general').select2({
            placeholder: "Select an option"
        });
    }

    var modalDemos = function() {
        $('#m_select2_modal').on('shown.bs.modal', function () {
            // basic
            $('#m_select2_1_modal').select2({
                placeholder: "Select a state"
            });

            // nested
            $('#m_select2_2_modal').select2({
                placeholder: "Select a state"
            });

            // multi select
            $('#m_select2_3_modal').select2({
                placeholder: "Select a state",
            });

            // basic
            $('#m_select2_4_modal').select2({
                placeholder: "Select a state",
                allowClear: true
            });
        });
    }

    //== Public functions
    return {
        init: function() {
            demos();
            modalDemos();
        }
    };
}();

//== Initialization
jQuery(document).ready(function() {
    Select2.init();
});
