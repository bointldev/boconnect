//== Class definition

var FormControls = function () {
    //== Private functions



    var clinetSubmit = function () {
        $( "#m_form_3" ).validate({
            // define validation rules
            rules: {
                //=== Client Information(step 3)
                //== Billing Information
                userfirstName: {
                    required: true
                },
                userPhone: {
                    required: true
                },
                user_address_1:{
                    required: true
                },
                user_state:{
                    required: true
                },
                user_city:{
                    required: true
                },
                billing_card_number: {
                    required: true,
                    creditcard: true
                },
                billing_card_exp_month: {
                    required: true
                },
                billing_card_exp_year: {
                    required: true
                },
                billing_card_cvv: {
                    required: true,
                    minlength: 2,
                    maxlength: 3
                },

                //== Billing Address
                billing_address_1: {
                    required: true
                },
                billing_address_2: {

                },
                billing_city: {
                    required: true
                },
                billing_state: {
                    required: true
                },
                billing_zip: {
                    required: true,
                    number: true
                },

                billing_delivery: {
                    required: true
                }
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                mUtil.scrollTo("m_form_3", -200);

                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
               // form[0].submit(); // submit the form
               $("#m_form_3").submit(function(event){
                event.preventDefault(); //prevent default action
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = new FormData(this); //Creates new FormData object
                $.ajax({
                    url : post_url,
                    type: request_method,
                    data : form_data,
                    contentType: false,
                    cache: false,
                    processData:false
                }).done(function(response){ //
                    console.log(response);
                    //$("#server-results").html(response);
                });
            });

                swal({
                    "title": "",
                    "text": "Client added successfully.!", 
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });

                return false;
            }
        });
    }

    return {
        // public functions
        init: function() {

            clinetSubmit();
        }
    };
}();

jQuery(document).ready(function() {
    FormControls.init();
});
