<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');

Route::get('/', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function() {

    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('users','UserController');
    Route::resource('boitems','BelowMaterialController');


    //client
    Route::resource('order','OrderController');
    Route::get('/orders/recived', 'OrderController@getOrderRecievedList')->name('order_recievedList');
    //client

    //staff
    Route::resource('client-Order','StaffOrderController');
    Route::get('/add/client', 'UserController@addNewClient')->name('addNewClient');
    Route::post('/saveClient', 'UserController@saveClient')->name('saveClient');
    Route::post('/clientAdd', 'UserController@clientAdd')->name('users.clientAdd');
    //staff

    Route::resource('products','ProductController');
    Route::get('/{slug}', 'UserController@getUserDashboard')->name('getUserDashboard');


});
