<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BelowMaterialStock extends Model
{
    protected $table = 'bo_material_stock';
    public $timestamps = false;
}
