<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BelowMaterial extends Model
{
    protected $table = 'bo_material';
    public $timestamps = false;
}
