<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Theme;
class OrderController extends Controller
{




    public function getOrderRecievedList(){
        $users_data='data';;      
        $theme = Theme::uses('users')->layout('layout');
        $data = ['data' =>$users_data];
        return $theme->scope('order.order_recievedList', $data)->render(); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            
        $users_data='data';;      
        $theme = Theme::uses('users')->layout('layout');
        $data = ['data' =>$users_data];
        return $theme->scope('order.index', $data)->render(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission ='data';
        $theme = Theme::uses('users')->layout('layout');
        $data = ['permission' => $permission];
        return $theme->scope('order.create', $data)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
