<?php

namespace App\Http\Controllers;

use App\BelowMaterial;
use Illuminate\Http\Request;
use App\User;
use Theme;
use App\Role;
use App\Helpers\AyraHelp;
use DB;

use App\BelowMaterialStock;
class BelowMaterialController extends Controller
{

//::bo_material_list
public function bo_material_list(Request $request){
    $items[] = array(
      'item_img' => 'https://www.w3schools.com//w3images//avatar6.png',
      'item_title' => 'Facewash',
      'item_price' => 2544.66,
      'item_info' => 'This is information',
    );
    $items[] = array(
      'item_img' => 'https://www.w3schools.com//w3images//avatar6.png',
      'item_title' => 'Facewash',
      'item_price' => 2544.66,
      'item_info' => 'This is information',
    );
    $item_response = array(
      'total_count' =>25 ,
      'incomplete_results' =>false,
      'items' =>$items
    );
    return response()->json($item_response);

}
//bo_material_list::

    //ajax call to get BO Material Items

public function get_bo_materials(Request $request){
$data_arr_material= DB::table('bo_material_stock')
->select('bo_material.item_name','bo_material.item_description','bo_material_stock.id',
'bo_material_stock.item_qty',
'bo_attr.attr_name',
'bo_attr_data.attr_value'
)
->join('bo_material','bo_material.id','=','bo_material_stock.bo_item_id')
->join('bo_attr','bo_attr.id','=','bo_material_stock.item_attribute')
->join('bo_attr_data','bo_attr_data.id','=','bo_material_stock.item_attribute_values')
->get();
    $data_arr = array();
    $i=0;
foreach ($data_arr_material as $key => $value) {
    $i++;


    $data_arr[]=array(
    'id' => $i,
    'item_id' => $value->id,
    'item_name' => $value->item_name,
    'item_description' =>  $value->item_description,
    'item_qty' =>  $value->item_qty,
    'attr_name' => $value->attr_name,
    'attr_value' =>  $value->attr_value,

    );
  }
  $resp_jon= json_encode($data_arr);
  $data = $alldata = json_decode($resp_jon);

  $datatable = array_merge(['pagination' => [], 'sort' => [], 'query' => []], $_REQUEST);

  // search filter by keywords
  $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch'])
      ? $datatable['query']['generalSearch'] : '';
  if ( ! empty($filter)) {
      $data = array_filter($data, function ($a) use ($filter) {
          return (boolean)preg_grep("/$filter/i", (array)$a);
      });
      unset($datatable['query']['generalSearch']);
  }

  // filter by field query
  $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
  if (is_array($query)) {
      $query = array_filter($query);
      foreach ($query as $key => $val) {
          $data = list_filter($data, [$key => $val]);
      }
  }

  $sort  = ! empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
  $field = ! empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'id';

  $meta    = [];
  $page    = ! empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
  $perpage = ! empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;

  $pages = 1;
  $total = count($data); // total items in array

  // sort
  usort($data, function ($a, $b) use ($sort, $field) {
      if ( ! isset($a->$field) || ! isset($b->$field)) {
          return false;
      }

      if ($sort === 'asc') {
          return $a->$field > $b->$field ? true : false;
      }

      return $a->$field < $b->$field ? true : false;
  });

  // $perpage 0; get all data
  if ($perpage > 0) {
      $pages  = ceil($total / $perpage); // calculate total pages
      $page   = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
      $page   = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
      $offset = ($page - 1) * $perpage;
      if ($offset < 0) {
          $offset = 0;
      }

      $data = array_slice($data, $offset, $perpage, true);
  }

  $meta = [
      'page'    => $page,
      'pages'   => $pages,
      'perpage' => $perpage,
      'total'   => $total,
  ];


  // if selected all records enabled, provide all the ids
  if (isset($datatable['requestIds']) && filter_var($datatable['requestIds'], FILTER_VALIDATE_BOOLEAN)) {
      $meta['rowIds'] = array_map(function ($row) {
          return $row->id;
      }, $alldata);
  }


  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
  header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

  $result = [
      'meta' => $meta + [
              'sort'  => $sort,
              'field' => $field,
          ],
      'data' => $data,
  ];

  echo json_encode($result, JSON_PRETTY_PRINT);

    }


    //ajax call to save data
    public function save_attr_values(Request $request){

        if (BelowMaterial::where('item_name', '=', $request->txtItems_name)->count() > 0) {
            // user found
            return response()->json([
                'data' => array(),
                'status' => 0,
                'message' => 'Items already exits'
            ]);

        }else{
            $bomaterial = new BelowMaterial;
            $bomaterial->item_name = $request->txtItems_name;
            $bomaterial->item_description = $request->txtItems_discription;
            $bomaterial->save();
            $curr_data=date('Y-m-d H:i:s');
            $bomaterial_stock = new BelowMaterialStock;
            $bomaterial_stock->bo_item_id =$bomaterial->id;
            $bomaterial_stock->item_qty =$bomaterial->id;
            $bomaterial_stock->item_attribute =$request->select_item_attr;
            $bomaterial_stock->item_attribute_values = $request->select_item_attr_values;
            $bomaterial_stock->updated_on =$curr_data;

            $bomaterial_stock->save();
            $bomaterial_stock->id;
            return response()->json([
                'data' => $bomaterial_stock,
                'status' => 1,
                'message' => 'Items addes succeffully'
            ]);

        }


    }

    //ajax call to get attribue items
    public function get_attr_items(Request $request){
        $getAttrItems=AyraHelp::getAttrItems($request->select_item_attr);
        return response()->json([
            'data' => $getAttrItems,
            'status' => 1,
            'message' => 'Get attribues successfully'
        ]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        $users_data=$users;
        $theme = Theme::uses('admin')->layout('layout');
        $data = ['data' =>$users_data];
        return $theme->scope('boitems.index', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getAttr = AyraHelp::getAttr();
        $theme = Theme::uses('admin')->layout('layout');
        $data = ['getAttr' => $getAttr];
        return $theme->scope('boitems.create', $data)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (BelowMaterial::where('item_name', '=', $request->txtItems_name)->count() > 0) {
            // user found
            return response()->json([
                'data' => array(),
                'status' => 0,
                'message' => 'Items already exits'
            ]);

        }else{
            $bomaterial = new BelowMaterial;
            $bomaterial->item_name = $request->txtItems_name;
            $bomaterial->item_description = $request->txtItems_discription;
            $bomaterial->save();
            $curr_data=date('Y-m-d H:i:s');
            $bomaterial_stock = new BelowMaterialStock;
            $bomaterial_stock->bo_item_id =$bomaterial->id;
            $bomaterial_stock->item_qty =$bomaterial->id;
            $bomaterial_stock->item_attribute =$request->select_item_attr;
            $bomaterial_stock->item_attribute_values = $request->select_item_attr_values;
            $bomaterial_stock->updated_on =$curr_data;

            $bomaterial_stock->save();
            $bomaterial_stock->id;
            return response()->json([
                'data' => $bomaterial_stock,
                'status' => 1,
                'message' => 'Items addes succeffully'
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BelowMaterial  $belowMaterial
     * @return \Illuminate\Http\Response
     */
    public function show(BelowMaterial $belowMaterial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BelowMaterial  $belowMaterial
     * @return \Illuminate\Http\Response
     */
    public function edit(BelowMaterial $belowMaterial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BelowMaterial  $belowMaterial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BelowMaterial $belowMaterial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BelowMaterial  $belowMaterial
     * @return \Illuminate\Http\Response
     */
    public function destroy(BelowMaterial $belowMaterial)
    {
        //
    }
}
