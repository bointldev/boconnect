<?php

namespace App\Http\Controllers;

use App\StaffOrder;
use Illuminate\Http\Request;
use Theme;
class StaffOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users_data='data';;
        $theme = Theme::uses('staff')->layout('layout');
        $data = ['data' =>$users_data];
        return $theme->scope('order.index', $data)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_()
    {
        $permission ='data';
        $theme = Theme::uses('staff')->layout('layout');
        $data = ['permission' => $permission];
        return $theme->scope('order.create', $data)->render();
    }
    public function create()
    {
        $permission ='data';
        $theme = Theme::uses('staff')->layout('order');
        $data = ['permission' => $permission];
        return $theme->scope('order.create', $data)->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StaffOrder  $staffOrder
     * @return \Illuminate\Http\Response
     */
    public function show(StaffOrder $staffOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StaffOrder  $staffOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(StaffOrder $staffOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StaffOrder  $staffOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaffOrder $staffOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StaffOrder  $staffOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(StaffOrder $staffOrder)
    {
        //
    }
}
