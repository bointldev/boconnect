<?php
//app/Helpers/AyraHelp.php
namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use App\Role;
class AyraHelp {


    //this is used to get att_values
    public static function getAttrItems($attr_id){
        $getAttrItems = DB::table('bo_attr_data')->where('attr_id', $attr_id)->get();
        return $getAttrItems;
    }
    public static function getAttr(){
        $getAttr = DB::table('bo_attr')->get();
        return $getAttr;
    }
    //this is used to get name of user
    public static function getEmail($user_id) {
        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->email) ? $user->email : '');
    }
    public static function getNewUserCount() {
        $user = DB::table('users')->where('is_read', '0')->get()->toArray();

        return count($user);
    }
    public static function getVendorName() {

      $users = User::role('vendor')->get();
      return $users;

    }
    public static function getBOMaterial() {
      $bo_material = DB::table('bo_material')->get();
      return $bo_material;
    }

//this function is used to get baseurl and route path
public static function getBaseURL(){
return url('/');
}
public static function getRouteName(){
    $route_arr= explode(url('/')."/",url()->current());
    if(array_key_exists(1,$route_arr)){
    return $route_arr[1];
    }

}

}
